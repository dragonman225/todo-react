# TODO-React

A simple to-do list web app written in ReactJS.

The Webpack starter is forked from https://github.com/StephenGrider/ReduxSimpleStarter.

### Getting Started

```
> git clone https://github.com/dragonman225/TODO-React.git
> cd TODO-React
> npm install
> npm start

```
### Bugs

* In TODO Item edit mode, if you edit the content but click the close(x) icon, the content will still be modified.
